#include <stdio.h>
#include <proc.h>
#include <unistd.h>

/*------------------------------------------------------------------------
 *  xthread_join  --- suspends the execution of the calling thread until 
 *  the thread xid terminates
 *------------------------------------------------------------------------
 */

int xthread_join(int xid, int *retval)
{
    /* waiting for a thread that does not exist. */
    join_queue_t *new; 

    /* disable the timer */
    int usec;
    usec = ualarm(0,0); 
    /* join a nonexisting thread */
    if(xtab[xid].xstate == XFREE)
        return(-2);
    /* join itself */
    if(xid == currxid)
        return(-1);

    /* join a zombie thread */
    if(xtab[xid].xstate == XZOMBIE)
    {
        *retval = xtab[xid].last_word;
        xtab[xid].xstate == XFREE;
    }
    else
    {
        /* suspend current thread */
        if(xtab[currxid].xstate == XRUN)
            xtab[currxid].xstate = XJOIN;
        else 
            printf("Error in suspending current thread, %d, print current state %d!\n", currxid, xtab[currxid].xstate);
        

        /*put current thread to the end of join waiting queue of xid*/
        new = malloc(sizeof(join_queue_t));  /* malloc a new queue thread for currxid */
        new->xid_join = currxid;
        new->next = NULL;

        
        /*inserting node , time complexity O(1)*/
        if (xtab[xid].head == NULL) /* No waiting queue */
        {   
            xtab[xid].head = new;
            xtab[xid].tail = new;
        }
            
        else  /*Has waiting queue */
        {
           xtab[xid].tail->next = new;
           xtab[xid].tail  = new; /*update tail to be new */
        }
        resched();
        *retval = xtab[new->xid_join].join_last_word;
    } 

    /* enable the timer */ 
    ualarm(usec, 0);
    return (0);
}


/*------------------------------------------------------------------------
 *  xthread_exit --- terminates the execution of the calling thread
 *------------------------------------------------------------------------
 */
void xthread_exit(int status)
{
    int usec, wait_xid;
    /* disable timer */
    usec = ualarm(0,0);
    
    /* has a waiting queue */
    if (xtab[currxid].head != NULL)
    {
    /* set all the waiting thread to READY */
        join_queue_t *t;  /* traveling node */
        t = xtab[currxid].head;
        while (t!= NULL)
        {
            wait_xid = t->xid_join;
            xtab[wait_xid].xstate = XREADY;
            xtab[wait_xid].join_last_word = status; 
            t = t->next;
        }
        xtab[currxid].head = NULL;
        xtab[currxid].tail = NULL;

        /* recycle current thread */
        xtab[currxid].xstate = XFREE;
        resched();
    }

    /* no waiting queue, set state to zombie */
    else 
    {
        xtab[currxid].xstate = XZOMBIE;
        xtab[currxid].last_word = status;
        resched();
    }
    
    /* enable timer */
    ualarm(usec, 0);
}


