#include <stdio.h>
/* test case2- foo joins bar, bar joins oof, oof exits. */
int xidfoo, xidbar, xidoof;
int retval1, retval2, retval3;
int x=0;

int foo(int f)
{
   int i;
   for(i=0;i<100;i++){
      printf("This is foo %d, %d\n", f, x++);
      // xthread_yield(xidbar);
   }
   xthread_join(xidbar, &retval1);
	printf("return val xidfoo %d\n", retval1);
}

int bar(int p, int q)
{
   int j;
   for(j=0;j<100;j++){
      printf("This is bar %d, %d\n", p-q, x++);
   }
   xthread_join(xidoof, &retval2);
	printf("return val xidbar %d\n", retval2);
	xthread_exit(11);
}

int oof(int o){
   int k;
   for(k=0;k<100;k++){
      printf("This is oof %d, %d\n", o, x++);
   }
   xthread_exit(22);
}

xmain(int argc, char* argv[])
{  
   printf("test case2- foo joins bar, bar joins oof, oof exits. \n");
   xidfoo = xthread_create(foo, 1, 7);
   xidbar = xthread_create(bar, 2, 32, 12);
   xidoof = xthread_create(oof, 1, 10);

   xthread_join(xidfoo, &retval1);
   printf("return to xmain\n");   
}
   
