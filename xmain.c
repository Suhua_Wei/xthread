#include <stdio.h>
int xidfoo, xidbar, xidoof;
int retval1, retval2, retval3;
int x=0;

int foo(int f)
{
   int i;
   for(i=0;i<100;i++){
      printf("This is foo %d, %d\n", f, x++);
      // xthread_yield(xidbar);
   }
   xthread_exit(22);
}

int bar(int p, int q)
{
   int j;
   for(j=0;j<100;j++){
      printf("This is bar %d, %d\n", p-q, x++);
      // xthread_yield(xidfoo);
   }
   xthread_exit(22);
}

int oof(int o){
   int k;
   for(k=0;k<100;k++){
      printf("This is oof %d, %d\n", o, x++);
      // xthread_yield(xidbar);
   }
   xthread_join(xidfoo, &retval3);
}



xmain(int argc, char* argv[])
{  
   xidfoo = xthread_create(foo, 1, 7);
   xidbar = xthread_create(bar, 2, 32, 12);
   xidoof = xthread_create(oof, 1, 10);

   xthread_join(xidfoo, &retval1);
   xthread_join(xidbar, &retval2);

   printf("return to xmain\n");
   printf("return val xidbar %d\n", retval2);
   printf("return val xidoof %d\n", retval3);
   printf("return val xidfoo %d\n", retval1);
}
   

