#include <stdio.h>
#include <unistd.h>

int xidfoo, xidbar,xidoof;

int foo(int f)
{
         int status;
         int ret;
         printf("***** Enter foo...\n");
         ret = xthread_join(xidoof,&status);
         printf("***** foo:  join oof status (54321) = %d and ret (0)= %d\n",status, ret);
}

int bar(int p, int q)
{
         int status;
         int ret;
         printf("***** Enter bar...\n");
         ret = xthread_join(xidoof,&status);
         printf("***** bar:  join oof status (54321) = %d and ret (0)= %d\n",status, ret);
}

int oof(int g)
{
         printf("***** oof: exit with status = %d\n",g);
         xthread_exit(g);
}

xmain(int argc, char* argv[])
{
    xidfoo = xthread_create(foo, 1, 7);
    xidbar = xthread_create(bar, 2, 32, 12);
    xidoof = xthread_create(oof,1,54321);
}