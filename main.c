#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <proc.h>


extern void xmain();
extern int  resched();

struct xentry xtab[10]; 
int currxid = 0; 

// Signal handler, tell system to call this function whenever it gets a SIGALRM
void handler(int sig)
{  
   sigset_t set;
   sigemptyset(&set);
   sigaddset(&set, SIGALRM);
   sigprocmask(SIG_UNBLOCK, &set, NULL);

   //suspend current runing thread 
   if(xtab[currxid].xstate ==XRUN)
      xtab[currxid].xstate = XREADY;
   
   //call resched to set next to run
   resched();
   }


main(int argc, char *argv[])
{
   register struct xentry *xptr;
   struct xentry m;
   int i;
   int xidxmain;

   //initialize processes
   for(i=0 ; i < NPROC; i++){
      xptr = &xtab[i];
      xptr->xid = i;
      xptr->xlimit =  (WORD) malloc(STKSIZE);
      xptr->xbase = xptr->xlimit + STKSIZE - sizeof(WORD);
   }
  
   /* the first thread runs user's xmain with id 0*/
   signal(SIGALRM, handler);
   ualarm(20000, 0);
   /*set singal at first 0.02 seconds*/
   
   xidxmain = xthread_create(xmain, 2, argc, argv);
   xtab[xidxmain].xstate = XRUN;
  
   ctxsw(m.xregs, xtab[xidxmain].xregs);
   /* never be here */
}
