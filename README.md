# README #
Project2 CIS620 


Authors: Suhua Wei & Taraneh Shamsaei

### How do I get set up? ###
Run:
$make

### Project description ####
1. State transition diagram  (attached seperately)



2. Program design
part 1.


part 2.
test case 1:
/*foo join oof, bar joins oof, abc joins oof, edf joins oof , oof exits. */

Output:

XT: Old threads never die; they just fade away. (id:0)
***** Enter foo...
***** Enter bar...
***** Enter abc...
***** Enter edf...
***** oof: exit with status = 22
***** foo:  join oof status (22) = 22 and ret (0)= 0
XT: Old threads never die; they just fade away. (id:1)
***** bar:  join oof status (22) = 22 and ret (0)= 0
XT: Old threads never die; they just fade away. (id:2)
***** abc: join off status (22) = 22 and ret (0) = 0
XT: Old threads never die; they just fade away. (id:3)
*****  edf: join off status (22) = 22 and ret (0) = 0
XT: Old threads never die; they just fade away. (id:4)
XT: no threads to run!

test case 2:
/*reverse order of test case 1 */

Output:
XT: Old threads never die; they just fade away. (id:0)
***** oof: exit with status = 22
***** Enter edf...
*****  edf: join off status (22) = 22 and ret (0) = 0
XT: Old threads never die; they just fade away. (id:2)
***** Enter abc...
***** abc: join off status (22) = 22 and ret (0) = 0
XT: Old threads never die; they just fade away. (id:3)
***** Enter bar...
***** bar:  join oof status (22) = 22 and ret (0)= 0
XT: Old threads never die; they just fade away. (id:4)
***** Enter foo...
***** foo:  join oof status (22) = 22 and ret (0)= 0
XT: Old threads never die; they just fade away. (id:5)
XT: no threads to run!



test case 3:
foo joins bar, bar joins oof, oof exits.

XT: Old threads never die; they just fade away. (id:0)
***** Enter foo...
***** Enter bar...
***** oof: exit with status = 10
***** bar:  join oof status (22) = 22 and ret (0)= 0
***** foo:  join bar status (22) = 22 and ret (0)= 0
XT: Old threads never die; they just fade away. (id:1)
XT: no threads to run!
