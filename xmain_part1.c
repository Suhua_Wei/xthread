#include <stdio.h>
#include <signal.h>

int xidfoo, xidbar;
int x=0;

void delay(int k){
   int s;
   for (s = 0; s < k*1000000; s++);

}

int foo(int f)
{
   int i;
   for(i=0;i<100;i++){
      printf("This is foo %d, %d\n", f, x++);
      delay(1);
   }
}

int bar(int p, int q)
{
   int j;
   for(j=0;j<100;j++){
      printf("This is bar %d, %d\n", p-q, x++);
      delay(1);
   }
}

xmain(int argc, char* argv[])
{
   xidfoo = xthread_create(foo, 1, 7);
   xidbar = xthread_create(bar, 2, 32, 12);
   delay(3);
}
